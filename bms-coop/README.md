# Black Mesa: COOP

Dockerfile builds a functioning Black Mesa: COOP server.

## Build & run

```bash
docker build -t bms-coop .
docker run -i -p 27015:27015/tcp -p 27015:27015/udp bms-coop
```

At the time of writing this, the final image size is roughly 31 GB.

## Notes

* The version of Metamod/Sourcemod required by Source:Coop refuses to load on newer distros, with no error explaining why (except for the common ELFCLASS64 error that appears even when metamod DOES load). This is the reason for using the ubuntu-18 image.
* Install libncurses5:i386 to eliminate the initial error that appears about ncurses not being installed, though I don't think this was breaking anything.
* Included server.cfg contains recommended mp_timelimit for COOP.

## References
* https://steamcommunity.com/sharedfiles/filedetails/?id=2200247356
* https://github.com/ampreeT/SourceCoop
