# Team Fortress 2 Dedicated Server

Dockerfile builds a functioning TF2 server.

## Build & run

```bash
docker build -t tf2-sm .
docker run -i -p 27015:27015/tcp -p 27015:27015/udp tf2-sm
```

At the time of writing this, the final image size is roughly 10 GB.

## Notes

See [other tf2 image](../tf2) for notes.
