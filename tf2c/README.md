# Team Fortress 2 Classic Dedicated Server

http://tf2classic.com/

Dockerfile builds a functioning TF2 Classic server.

## Build & run

Use your preferred method to download tf2-classic.zip and place in this directory. A list of official download mirrors and Torrents can be found in Discord.

```bash
docker build -t tf2c .
docker run -i -p 27015:27015/tcp -p 27015:27015/udp tf2c
```

At the time of writing this, the final image size is roughly 18 GB.

